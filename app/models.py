from app import db, login_manager,ma
from app import bcrypt
from flask_login import UserMixin
from marshmallow_sqlalchemy import ModelSchema
from marshmallow import fields ,Schema
from flask_bcrypt import generate_password_hash, check_password_hash

@login_manager.user_loader
def load_user(user_id):
    return User.query.get(user_id)

class User(db.Model, UserMixin):
    id = db.Column(db.Integer(), primary_key=True)
    username = db.Column(db.String(length=30), nullable=False, unique=True)
    email_address = db.Column(db.String(length=50), nullable=False, unique=True)
    password_hash = db.Column(db.String(length=60), nullable=False)
    
    blog = db.relationship('Blog', backref='owned_user', lazy=True)
    def __init__(self, username, email_address ,password_hash):
        self.username = username
        self.email_address = email_address
        self.password_hash=password_hash
    '''def get_id(self):
        """Return the email address to satisfy Flask-Login's requirements."""
        return self.username'''
    @property
    def password(self):
        return self.password

    @password.setter
    def password(self, plain_text_password):
        self.password_hash = bcrypt.generate_password_hash(plain_text_password).decode('utf-8')
    
    def check_password_correction(self, attempted_password):
        return bcrypt.check_password_hash(self.password_hash, attempted_password)
    def check_password(self, password1):
        return check_password_hash(self.password_hash, password1)

class Blog(db.Model):
    id = db.Column(db.Integer(), primary_key=True)
    title = db.Column(db.String(length=30), nullable=False, unique=True)
    
    description = db.Column(db.String(length=1024), nullable=False, unique=True)
    owner = db.Column(db.Integer(), db.ForeignKey('user.id'))
    def __repr__(self):
        return f'Item {self.title}'

class BlogpostSchema(ModelSchema):
    class Meta:
        fields = ('title','description')
blog_schema = BlogpostSchema()
blogss_schema = BlogpostSchema(many=True)
class UserSchema(ma.Schema):
    class Meta:
        # Fields to expose
        fields = ('username', 'email_address','password_hash')


user_schema = UserSchema()
users_schema = UserSchema(many=True)


    
    
