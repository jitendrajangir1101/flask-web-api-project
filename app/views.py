from flask import render_template ,redirect ,flash , url_for, session ,request,make_response
from app.models import Blog ,User,user_schema,users_schema,UserSchema,BlogpostSchema,blog_schema,blogss_schema
from app import db
from flask import Flask, jsonify, request
from app import app
from app.forms import RegisterForm ,LoginForm ,BlogForm
from flask_login import logout_user ,login_user,current_user
import datetime
from flask_jwt_extended import create_access_token
from flask_jwt_extended import jwt_required,get_jwt_identity
import logging


@app.route('/', methods=['GET'])
def index():
    app.logger.info('Processing default request to home page')
    blogs = Blog.query.all()
    return render_template("index.html" ,blogs=blogs)

@app.route('/profile', methods=['GET'])
def profile():
    if 'username' in session:  
        blogs = Blog.query.filter_by(owner =current_user.id)
        return render_template("profile.html",blogs=blogs )
        
    else: 
        flash("Please login first","success")
        return redirect(url_for('loginUser'))  
         
    






# other imports as necessary
@app.route("/logout")
def logout():
    if 'username' in session:  
        session.pop('username',None) 
        logout_user()
        flash("you are logged out ","success")
        return redirect(url_for('index')) 
         
    else:  
        flash("user already logged out ","success")
        return redirect(url_for('index'))
    

@app.route('/about')
def about():
    return render_template("about.html")

@app.route('/register', methods=['GET', 'POST'])
def register():
    form = RegisterForm()
    if form.validate_on_submit():
        user_to_create = User(username=form.username.data,
                              email_address=form.email_address.data,
                              password=form.password1.data)
        db.session.add(user_to_create)
        db.session.commit()
        app.logger.info('%s User Registered successfully', user_to_create.username)
        return redirect(url_for('profile'))
    if form.errors != {}: #If there are not errors from the validations
        for err_msg in form.errors.values():
            flash(f'There was an error with creating a user: {err_msg}', category='danger')
            app.logger.info('%s User not Registered successfully', form.username.data)

    return render_template('register.html', form=form)

@app.route('/login', methods=['GET', 'POST'])
def loginUser():
    form = LoginForm()
    if form.validate_on_submit():
        
        attempted_user = User.query.filter_by(username=form.username.data).first()
        if attempted_user and attempted_user.check_password_correction(
                attempted_password=form.password.data
        ):
            login_user(attempted_user)
            app.logger.info('%s logged in successfully', user.username)
            session['username']=request.form['username']
            print(session['username'])
            session.permanent = True
            flash(f'Success! You are logged in as: {attempted_user.username}', category='success')
            return redirect(url_for('profile'))
        else:
            flash('Username and password are not match! Please try again', category='danger')
            app.logger.info('%s failed to log in', user.username)

    return render_template('login.html', form=form)


@app.route('/addblog', methods=['GET', 'POST'])
def addblog():
    if 'username' in session:
        form = BlogForm()
        
        if form.validate_on_submit():
            blog = Blog(title=form.title.data,
                                description=form.description.data,
                                owner = current_user.id, )
            db.session.add(blog)
            db.session.commit()
            app.logger.info('%s Blog Added by ', current_user.username)
            return redirect(url_for('profile'))
        
            
        return render_template("addblog.html" ,form=form)
    else:
        flash("Your Session Expired , Please login first","success")
        return redirect(url_for('loginUser'))  

@app.route('/deleteBlog/<int:id>',methods=['GET'])
def deleteBlog(id):
    if 'username' in session:
        blog = Blog.query.get(id)
        db.session.delete(blog)
        db.session.commit()
        app.logger.info('%s Blog Deleted Successfully',blog.id )
        flash("Post has been deleted" ,'success')
        return redirect(url_for('profile'))
    else:
        flash("Your Session Expired , Please login first","success")
        return redirect(url_for('loginUser'))  

@app.route('/editBlog/<int:id>',methods=['GET','POST'])
def editBlog(id):
    if 'username' in session:
        blog = Blog.query.get(id)
        if request.method == "POST":
            blog.title=request.form.get('title')
            blog.description=request.form.get('description')
            blog.owner = current_user.id
            
            db.session.commit()
            app.logger.info('%s Blog Updated successfully', blog.id)
            flash("Your Blog updated ","success")
            return redirect(url_for('profile'))
        return render_template("editBlog.html",blog=blog)
    else:
        flash("Your Session Expired , Please login first","success")
        return redirect(url_for('loginUser'))

    
    


    


# endpoint to show all users
@app.route("/user", methods=["GET"])
@jwt_required()
def get_user():
    all_users = User.query.all()
    app.logger.info('%s User list Viewed By', get_jwt_identity())
    result = users_schema.dump(all_users)
    return jsonify(result)


# endpoint to get user detail by id
@app.route("/user/<id>", methods=["GET"])
@jwt_required()
def user_detail(id):
    user = User.query.get(id)
    app.logger.info('%s User Detail Viewed By', get_jwt_identity())
    return user_schema.jsonify(user)

'''
# these Api can be used for admin purpose.
# endpoint to update user
@app.route("/user/<id>", methods=["PUT"])
@jwt_required()
def user_update(id):
    user = User.query.get(id)
    username = request.json['username']
    email_address = request.json['email_address']
    password_hash=request.json['password_hash']

    user.email_address= email_address
    user.username = username
    user.password_hash=password_hash

    db.session.commit()
    app.logger.info('%s User Info Updated  By', get_jwt_identity())
    return user_schema.jsonify(user)


# endpoint to delete user
@app.route("/deleteUser/<id>", methods=["DELETE"])
@jwt_required(id)
def user_delete():
    
    user = User.query.get(id)
    db.session.delete(user)
    db.session.commit()
    app.logger.info('%s User Info Deleted By', get_jwt_identity())

    return user_schema.jsonify(user)
    
    '''


@app.route('/loginApi',methods=['POST'])
def loginApi():
   body = request.get_json()
   print(body.get('username'))
   user = User.query.filter_by(username=body.get('username')).first()
   authorized = user.check_password(body.get('password'))
   if not authorized:
     return {'error': 'Email or password invalid'}, 401
 
   expires = datetime.timedelta(days=7)
   access_token = create_access_token(identity=str(user.id), expires_delta=expires)
   app.logger.info('%s User Logged in ', get_jwt_identity())
   return {'token': access_token,'message':'logged in','id':user.id}, 200


# Blog api for create ,list , update and delete
# Get All Blogs
@app.route("/blogList", methods=["GET"])
@jwt_required()
def get_blog():
    all_blogs = Blog.query.all()
    result = blogss_schema.dump(all_blogs)
    app.logger.info('%s All Blogs viewd by ', get_jwt_identity())
    return jsonify(result)
#Get blog of logged in user.
@app.route("/blogListOfUser", methods=["GET"])
@jwt_required()
def get_blog_of_user():
    all_blogs = Blog.query.filter_by(owner=int(get_jwt_identity()))
    result = blogss_schema.dump(all_blogs)
    app.logger.info('%s Blog viewd by ', get_jwt_identity())
    return jsonify(result)

#Update blog of a logged in user
@app.route("/blogUpdate/<id>", methods=["PUT"])
@jwt_required()
def blogUpdate(id):
    blog = Blog.query.get(id)
    if blog.owner == int(get_jwt_identity()):
        title = request.json['title']
        description = request.json['description']
        

        blog.title= title
        blog.description = description
        blog.owner = int(get_jwt_identity())

        db.session.commit()
        app.logger.info('%s Blog Updated By', get_jwt_identity())
        return blog_schema.jsonify(blog)
    else :
        app.logger.info('%s Blog not Updated successfuly', get_jwt_identity())
        return {'Message': 'This blog does not belongs to you '}
#Delete blog of logged in user.
@app.route("/deleteBlog/<id>", methods=["DELETE"])
@jwt_required()
def delete_blog(id):
    
    blog = Blog.query.get(id)
    if blog:
        print(blog)
        print(blog.owner)
        print(get_jwt_identity())
        if blog.owner == int(get_jwt_identity()):
            db.session.delete(blog)
            db.session.commit()
            app.logger.info('%s Blog Deleted By', get_jwt_identity())

            return {'Message': 'This blog deleted '}
        else :
            return {'Message': 'This blog does not belongs to you '}
    else:
        app.logger.info('%s Blog not exist', get_jwt_identity())
        return {'Message': 'Blog does not exist '}
