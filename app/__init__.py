from flask import Flask, render_template
from flask_sqlalchemy import SQLAlchemy
from flask_bcrypt import Bcrypt
from flask_login import LoginManager
import connexion
from flask_marshmallow import Marshmallow
from flask_restful import reqparse, Api, Resource
from flask_jwt_extended import JWTManager
from datetime import timedelta
import logging

# Create the application instance
app = connexion.App(__name__, specification_dir='./')

# Read the swagger.yml file to configure the endpoints
#app.add_api('swagger.yml')
app = Flask(__name__)
logging.basicConfig(filename='demo.log', level=logging.DEBUG ,format='%(asctime)s %(levelname)s %(name)s %(threadName)s : %(message)s')
app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///blog.db'
db = SQLAlchemy(app)
ma = Marshmallow(app)
app.config['SECRET_KEY'] = 'ec9439cfc6c796ae2029594d'
JWT_SECRET_KEY = 't1NP63m4wnBg6nyHYKfmc2TpCOGI4nss'
app.config['PERMANENT_SESSION_LIFETIME'] =  timedelta(minutes=2)
bcrypt =Bcrypt(app)
login_manager = LoginManager(app)
jwt = JWTManager(app)
# Load the views
from app import views
from app import models

# Load the config file
app.config.from_object('config')